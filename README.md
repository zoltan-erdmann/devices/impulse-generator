# Impulse generator


## Limits of the device

The device can generate impulses between 0.1 - 2000 Hz.


## Connection parameters

| Name       | Value |
|------------|-------|
| Baud rate  | 9600  |
| Data bits  | 8     |
| Stop bits  | One   |
| Parity     | None  |
| Handshake  | None  |
| New line   | \r    |

You should send out a new line (`\r`) character at the beginning of the communication.


## Commands

You must terminate all commands with the new line (`\r`) character.

### Getting the Version

Command: `v`
Arguments: -


#### Example

You can get the version by submitting the following command:

```
v
```

The response can contain the following parameters of the device:
* Version
* Device identifier
* Manufacturing time
* Serial number


### Sentting impulses

Command: `p`
Arguments:
The arguments are separated by a single "` `" (space) character.

* Channel - The first channels number is 1.
* Count
* Period length


#### Example

You can send out 10 impulses with the period length of 200 on channel 1 by submitting the following command:

```
p 1 10 200
```


## Events sent by the device


### Channel finnished

When a chennel sent oult all the impulses it sends out a message that indicates the channel that has completed its task.

#### Format


```
c <channel number>
```


##### Example


```
c 1
```
