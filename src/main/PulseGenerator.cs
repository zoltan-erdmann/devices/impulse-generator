﻿// SPDX-License-Identifier: LGPL-3.0-only

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO.Ports;
using System.Threading;
using FFW.NSDevices;
using System.Globalization;

namespace FFW.Devices.NSPulseGenerator {

	public delegate void ChannelReady(PulseGenerator sender, ImpulseReadyEventArgs args);

	public class PulseGenerator : IRS232Device {

		public event ChannelReady ChannelReadyEvent;

		//private volatile List<byte> _receiveBuffer;
		private volatile StringBuilder _receiveBuffer;
		private SerialPort _serialPort;
		private byte _newLine;
		private AutoResetEvent _functionSyncEvent = new AutoResetEvent(false);
		private bool[] _channelReadyStates = { false, false, false, false };
		private bool _triggerFinished;
		private object _channelReadyLock = new object();
		private float _minValue;
		private float _maxValue;
		private object _portLock = new object();

		public string Name { get; private set; }
		public string ManufacturerName { get { return ConstantPropertiesResource.ManufacturerName; } }

		public int ChannelCount { get { return 4; } }
		public string Version { get; private set; }
		public int ID { get; private set; }
		public DateTime ReleaseDate { get; private set; }
		public string SerialNumber { get; private set; }
		public float MinValue { get { return _minValue; } }
		public float MaxValue { get { return _maxValue; } }
		public string Unit { get { return ConstantPropertiesResource.Unit; } }

		public string PortName { get { return _serialPort.PortName; } }
		public int BaudRate { get { return _serialPort.BaudRate; } }
		public Parity Parity { get { return _serialPort.Parity; } }
		public int DataBits { get { return _serialPort.DataBits; } }
		public StopBits StopBits { get { return _serialPort.StopBits; } }

		private void TriggerChannelReadyEvent(ImpulseReadyEventArgs e) {
			if(ChannelReadyEvent != null) {
				ChannelReadyEvent(this, e);
			}
		}

		private void serialPort_DataReceived(object sender, SerialDataReceivedEventArgs e) {
			SerialPort sp = sender as SerialPort;
			List<byte> localBuffer = new List<byte>();
			lock(_portLock) {
				if(sp == null && !sp.IsOpen) { return; }
				/*int length = sp.BytesToRead;
				while(length > 0) {
					byte[] buffer = new byte[length];
					sp.Read(buffer, 0, length);
					localBuffer.AddRange(buffer);
					length = sp.BytesToRead;
				}*/

				_receiveBuffer.Append(sp.ReadExisting());
			}
			//_receiveBuffer = _receiveBuffer.Concat(localBuffer).ToList();
			//_latestIOTime = DateTime.Now;
			//ClearReceivedData();
			//TriggerDataReceived(localBuffer.ToArray());
			/*if(IsValidMessage(_receiveBuffer.ToArray())) {
				byte[] response = _receiveBuffer.ToArray();
				TriggerMessageReceived(response);
				_receiveBuffer.Clear();
			}



			SerialPort sp = (SerialPort)sender;
			
			while(sp.IsOpen && sp.BytesToRead > 0) {
				_receiveBuffer.Add((byte)sp.ReadByte());
			}*/
			string rba = _receiveBuffer.ToString();
			while(rba.Contains('\r')) {
				string response = rba.Substring(0, rba.IndexOf('\r') + 1);
				_receiveBuffer.Clear();
				_receiveBuffer.Append(rba.Substring(rba.IndexOf('\r') + 1));
				rba = rba.Substring(rba.IndexOf('\r') + 1);
				if(response.Trim().StartsWith("unknown")) {
					System.Diagnostics.Trace.WriteLine(string.Format("đđđđđđđđ  unknown"));
					continue;
				}
				/*byte[] bytes = _receiveBuffer.GetRange(0, _receiveBuffer.IndexOf(_newLine) + 1).ToArray();
				_receiveBuffer.RemoveRange(0, _receiveBuffer.IndexOf(_newLine) + 1);
				System.Text.Encoding enc = System.Text.Encoding.ASCII;
				string response = enc.GetString(bytes);*/
				System.Diagnostics.Trace.WriteLine(string.Format("đđđđđđđđ  [{0}|{1}]", response.Trim(), _receiveBuffer.ToString().Trim()));
				int channel;
				if(response.Length > 3) {
					if(response.StartsWith("c") && Int32.TryParse(response.Substring(2, 1), out channel)) {
						lock(_channelReadyLock) {
							_channelReadyStates[channel - 1] = true;
							bool isAllChannelFinished = _channelReadyStates.Count<bool>(x => { return x.Equals(false); }) == 0;
							TriggerChannelReadyEvent(new ImpulseReadyEventArgs(channel, _triggerFinished && isAllChannelFinished));
							if(isAllChannelFinished) {
								_triggerFinished = false;
							}
						}
					} else {
						response = response.Trim();
						if(response.Contains("Version")) {
							Name = response.Substring(0, response.IndexOf(" "));
							Version = response.Substring(response.IndexOf("Version ")).Replace("Version ", "");
						} else if(response.Contains("Device identifier:")) {
							int id = -1;
							Int32.TryParse(response.Substring(response.IndexOf("Device identifier:")).Replace("Device identifier: ", ""), out id);
							ID = id;
						} else if(response.Contains("created on")) {
							DateTime releaseDate;
							if(DateTime.TryParse(response.Substring(response.IndexOf("created on")).Replace("created on ", ""), out releaseDate)) {
								ReleaseDate = releaseDate;
							}
							_functionSyncEvent.Set();
						} else if(response.Contains("Serial number")) {
							SerialNumber = string.Format("{0}/{1}", response.Substring(14, 2), response.Substring(16));
							//Version = response.Substring(response.IndexOf("Version ")).Replace("Version ", "");
							System.Diagnostics.Trace.WriteLine(string.Format("đđđđđđđđ  SN SET"));
							_functionSyncEvent.Set();
						}
						
					}
				} else {
					System.Diagnostics.Trace.WriteLine(
						string.Format("Unexpected response from impulse generator: '{0}'", response));
				}
			}
			
		}

		public PulseGenerator(string portName) {
			//_receiveBuffer = new List<byte>();
			_serialPort = new SerialPort(portName);
			_serialPort.Parity = Parity.None;
			_serialPort.DataBits = 8;
			_serialPort.StopBits = StopBits.One;
			_serialPort.BaudRate = 9600;
			_serialPort.Handshake = Handshake.None;
			_serialPort.NewLine = "\r";
			_serialPort.DataReceived += new SerialDataReceivedEventHandler(serialPort_DataReceived);
			_newLine = BitConverter.GetBytes('\r')[0];

			CultureInfo ci = (CultureInfo)CultureInfo.CurrentCulture.Clone();
			ci.NumberFormat.NumberDecimalSeparator = ".";
			_minValue = float.Parse(ConstantPropertiesResource.MinValue, ci);
			_maxValue = float.Parse(ConstantPropertiesResource.MaxValue, ci);
		}

		public void Open() {
			_receiveBuffer = new StringBuilder();
			_serialPort.Open();
			_serialPort.WriteLine("\r");
			_serialPort.WriteLine("v");
			System.Diagnostics.Trace.WriteLine(string.Format("đđđđđđđđ  WAIT"));
			if(!_functionSyncEvent.WaitOne(3000)) {
				System.Diagnostics.Trace.WriteLine(string.Format("đđđđđđđđ  EXCEPT"));
				throw new TimeoutException("Failed to connect to device!");
			}
			if(SerialNumber == null) {
				_serialPort.WriteLine("v");
				System.Diagnostics.Trace.WriteLine(string.Format("đđđđđđđđ  WAIT"));
				if(!_functionSyncEvent.WaitOne(3000)) {
					System.Diagnostics.Trace.WriteLine(string.Format("đđđđđđđđ  EXCEPT"));
					throw new TimeoutException("Failed to connect to device!");
				}
			}
		}

		public void Close() {
			lock(_portLock) {
				_serialPort.Close();
			}
		}

		public void SetImpulse(int channel, int count, int periodLength) {
			Thread.Sleep(5);
			_triggerFinished = true;
			_serialPort.WriteLine(string.Format("p {0} {1} {2}", channel, count, periodLength));
		}
	}
}
