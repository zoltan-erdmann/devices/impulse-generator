﻿// SPDX-License-Identifier: LGPL-3.0-only

namespace FFW.Devices.NSPulseGenerator {

	public class ImpulseReadyEventArgs {

		public int ChangedChannel { get; private set; }
		public bool IsAllChannelsReady { get; private set; }

		public ImpulseReadyEventArgs(int changedChannel, bool isAllChannelsReady) {
			ChangedChannel = changedChannel;
			IsAllChannelsReady = isAllChannelsReady;
		}
	}
}
